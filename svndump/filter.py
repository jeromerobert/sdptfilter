from optparse import OptionParser
import re
from svndump import __version
from file import SvnDumpFile
from node import SvnDumpNode

class Filter:
    def __init__( self ):
        """
        Initialize.
        """

        # input file name
        self.__in_file = ""
        # output file name
        self.__out_file = ""
        # dry-run, do not create an output file
        self.__dry_run = True
        self.__extensions = []
        self.__regex = []

    def set_input_file( self, filename ):
        """
        Sets the input dump file name.

        @type filename: string
        @param filename: Name of the input file.
        """
        self.__in_file = filename

    def set_output_file( self, filename ):
        """
        Sets the output dump file name and clears the dry-run flag.

        @type filename: string
        @param filename: Name of the output file.
        """
        self.__out_file = filename
        self.__dry_run = False

    def set_extensions( self, ext ):
        """
        Sets the list of extensions to remove from the dump
        
        @type ext: string
        @param ext: comma separated list of extension
        """
        self.__extensions=ext.split(",")

    def set_regexpfile( self, file ):
        for r in open(file, "r"):
            s = r.rstrip('\n').rstrip('\r')
            if len(s.strip()) != 0:
                self.__regex.append(re.compile(s))

    def __get_extension(self, filename):
        """
        Return the extension of a file
        """
        s = filename.split(".")
        if len(s) <= 1:
            return None
        else:
            return s[-1]
        
    def execute( self ):
        """
        Executes the filter.
        """

        # +++ catch exception and return errorcode
        srcdmp = SvnDumpFile()
        srcdmp.open( self.__in_file )

        dstdmp = None

        hasrev = srcdmp.read_next_rev()
        if hasrev:
            if not self.__dry_run:
                dstdmp = SvnDumpFile()
                if srcdmp.get_rev_nr() == 0:
                    # create new dump with revision 0
                    dstdmp.create_with_rev_0( self.__out_file,
                                              srcdmp.get_uuid(),
                                              srcdmp.get_rev_date_str() )
                    hasrev = srcdmp.read_next_rev()
                else:
                    # create new dump starting with the same revNr
                    # as the input dump file
                    dstdmp.create_with_rev_n( self.__out_file,
                                              srcdmp.get_uuid(),
                                              srcdmp.get_rev_nr() )
            # now copy all the revisions
            while hasrev:
                print "\n\n*** r%d ***\n" % srcdmp.get_rev_nr()
                self.__process_rev( srcdmp, dstdmp )
                hasrev = srcdmp.read_next_rev()

    def __match_regex( self, filename):
        for r in self.__regex:
            if r.search( filename ) != None:
                 return True
        return False
 
    def __process_rev( self, srcdmp, dstdmp ):
        """
        Process one revision.

        @type srcdmp: SvnDumpFile
        @param srcdmp: The source dump file.
        @type dstdmp: SvnDumpFile
        @param dstdmp: The destination dump file.
        """

        # clear temp file nr (overwrite old files)
        self.__temp_file_nr = 0

        # add revision and revprops
        if dstdmp != None:
            dstdmp.add_rev( srcdmp.get_rev_props() )

        # process nodes
        index = 0
        nodeCount = srcdmp.get_node_count()
        while index < nodeCount:
            node = srcdmp.get_node( index )
            isFile = node.get_kind()=='file'
            matchExt = self.__get_extension(node.get_path()) in self.__extensions
            if ( isFile and matchExt ) or self.__match_regex( node.get_path() ):
                print "remove " + node.get_path()
            else:
                if dstdmp != None:
                     dstdmp.add_node( node )
            index = index + 1


def svndump_filter_cmdline( appname, args ):
    """
    Parses the commandline and executes the filter.

    Usage:

        >>> svndump_filter_cmdline( sys.argv[0], sys.argv[1:] )

    @type appname: string
    @param appname: Name of the application (used in help text).
    @type args: list( string )
    @param args: Commandline arguments.
    @rtype: integer
    @return: Return code (0 = OK).
    """

    usage = "usage: %s [options] src [dst]" % appname
    parser = OptionParser( usage=usage, version="%prog "+__version )
    parser.add_option( "-e", "--extension",
                       action="append", dest="extensions",
                       type="string",
                       help="A comma seperated list of file extensions"
                       " to remove from the dump")

    parser.add_option( "-r", "--regexp-file",
                       action="append", dest="regexpfile",
                       type="string",
                       help="A file containing one regexp by line. "
                       "Matching files will be removed from the dump.")

    (options, args) = parser.parse_args( args )

    filter = Filter()

    if len( args ) < 1 or len( args ) > 2:
        print "please specify one source and optionally one destination file."
        return 1
    filter.set_input_file( args[0] )
    if len( args ) == 2:
        filter.set_output_file( args[1] )
    if options.extensions != None:
        filter.set_extensions( options.extensions[0] )
    if options.regexpfile != None:
        filter.set_regexpfile( options.regexpfile[0] )
    filter.execute()
    return 0

